import pylab
import numpy as np

def Vprime_bistable(x):
    """
    Returns the value of the derivative of the function V().
    """
    return x**3-x
    
def V(x):
    """
    Returns the potential function.
    """
    return x**4/4 - x**2/2
    
def rho_hat(x,sigma=0.5,C=1):      
    """
    This functions returns the pdf needed for this lab.
    """      
    return C*np.exp(-2./np.power(sigma,2)*V(x))   
   
def getEnsemble(N,x,sigma,dt,T,tdump):
    """
    Integrates a system (brownian_motion) for N times and return an array of X_t.
    
    Inputs: 
    N - ensemble size
    x - initial condition
    sigma - constant
    dt - time step size
    T - simulation time limit
    
    Returns:
    Ensemble array
    """
    Ens = np.zeros(N)
    for i in range(len(Ens)):
        _x, dummy = brownian_dynamics(x,Vprime_bistable,sigma,dt,T,tdump)                
        Ens[i] = _x[-1]
        
    return Ens
    
def MC(func,sigma,dt,T,td):
    """
    Inputs:
    func - the name of the function to be used by the Monte-Carlo for calculating the ergodic average
    sigma, dt,T,td - constants, as before
    
    Returns:
    A Monte-Carlo approximation
    """
    _x,_t = brownian_dynamics(0.,Vprime_bistable,sigma,dt,T,td)    
    fx = func(_x)      
    return np.cumsum(fx)/np.cumsum(np.ones(len(fx)))

def brownian_dynamics(x,Vp,sigma,dt,T,tdump):
    """
    Solve the Brownian dynamics equation

    dx = -V(x)dt + sigma*dW

    using the Euler-Maruyama method

    x^{n+1} = x^n - V(x^n)dt + sigma*dW

    inputs:
    x - initial condition for x
    Vp - a function that returns V'(x) given x
    dt - the time stepsize
    T - the time limit for the simulation
    tdump - the time interval between storing values of x

    outputs:
    xvals - a numpy array containing the values of x at the chosen time points
    tvals - a numpy array containing the values of t at the same points
    """

    xvals = [x]
    t = 0.
    tvals = [0]
    dumpt = 0.
    while(t<T-0.5*dt):
        dW = dt**0.5*np.random.randn()
        x += -Vp(x)*dt + sigma*dW

        t += dt
        dumpt += dt

        if(dumpt>tdump-0.5*dt):
            dumpt -= tdump
            xvals.append(x)
            tvals.append(t)
    return np.array(xvals), np.array(tvals)

if __name__ == '__main__':
    dt = 0.001
    sigma =0.5
    T = 2000.0
    
    #-------------------- Ex 2-------------------------   
    dx = 0.05    
    x = np.arange(-2,2,dx)
    #Calculate the constant: C ~= 1/sum(f(x))*dx. Needs to be recalculated
    #every time sigma or dx are changed
    C = 1/(np.sum(rho_hat(x,sigma,1))*dx)
    #Recalculat the pdf
    pdf = rho_hat(x,sigma,C)
    
    print 'C =',C
    
    for i in range(10,160,30):
        ens = getEnsemble(500,1.,sigma,0.1,i,0.1)
        
        pylab.hist(ens,60,normed=True,label='Approx pdf')
        pylab.plot(x,pdf, '-r',label='Exact pdf')
        pylab.title('T='+str(i)+', $\sigma$='+str(sigma))
        pylab.xlabel('x')
        pylab.ylabel('pdf')
        pylab.legend(loc='best')
        pylab.show()
    
    print 'The rate of the convergence is now faster that we have a noise\
          term in. Furthermore, a large enough integration time is needed\
          in order to approximate the pdf to a certain degree.'
    #--------------------------------------------------
    
    #-------------------- Ex 3-------------------------   
    #Define a list of lambda functions here 
    Lfuncs =['lambda x: x','lambda x:x**2']
    names=['$x$','$x^2$']    
    
    for i in range(len(Lfuncs)):
        a = MC(eval(Lfuncs[i]), sigma,dt,40000,1)
        
        #For f(x)=x, E[f(x)] = 0 but might not be so numerically except for N -> inf
        #Calculate the expectation value  !! Condensed coding below !!
        E = np.sum(eval(Lfuncs[i])(x)*rho_hat(x,sigma,C))*dx
        print 'For',Lfuncs[i],' E[f(x)]=',E
                
        pylab.plot(range(len(a)),a)
        pylab.xlabel('N')    
        pylab.ylabel('MC')
        pylab.title('MC convergence for $f(x)=$'+names[i])
        pylab.show()
        
        pylab.semilogy(range(len(a)),np.abs(a-E))        
        pylab.xlabel('log(N)')
        pylab.ylabel('log(MC)')
        pylab.title('Error for $f(x)=$'+names[i])
        pylab.show()
        
    print 'For both functions MC converges to the expected value as N increases.'
    print 'On the lin-log plot the convergence looks, if one does squint, linear which indicates an exponential behaviour'
    print 'With the excuse that today is the end of term and also I had wine and beer, I shall not calculate the rate of convergence. Apologies.'
    
    #--------------------------------------------------

