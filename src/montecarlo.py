import numpy as np
import pylab

def square(x):
   """
   Function to compute the square of the input. If the input is
   a numpy array, this returns a numpy array of all of the squared values.
   inputs:
   x - a numpy array of values

   outputs:
   y - a numpy array containing the square of all of the values
   """
   return x**2

def normal(N):
   """
   Function to return a numpy array containing N samples from
   a N(0,1) distribution.
   """
   return np.random.randn(N)

def montecarlo(f, X, N):
   """
   Function to compute the Monte Carlo estimate of the expectation
   E[f(X)], with N samples

   inputs:
   f - a Python function that applies a chosen mathematical function to
   each entry in a numpy array
   X - a Python function that takes N as input and returns
   independent individually distributed random samples from a chosen
   probability distribution
   N - the number of samples to use
   """

   theta = np.sum(f(X(N)))/N
   error_estimate = 0.0
   return theta, error_estimate

if __name__ == '__main__':    
    theta, err = montecarlo(square, normal, 10000)
    print theta
