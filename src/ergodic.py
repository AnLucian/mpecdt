import numpy as np
import pylab
from scipy import integrate
from data import getdata
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def vectorfield(x, t, sigma, rho, beta):
    """
    Function to return the value of dx/dt, given x.
    
    Inputs:
    x - the value of x
    t - the value of t
    alpha - a parameter
    beta - another parameter

    Outputs:
    dxdt - the value of dx/dt
    """
    
    return np.array([sigma*(x[1]-x[0]),
                        x[0]*(rho-x[2])-x[1],
                        x[0]*x[1]-beta*x[2]])

def ensemble_plot(Ens,title=''):
    """
    Function to plot the locations of an ensemble of points.
    """    

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(Ens[:,0],Ens[:,1],Ens[:,2],'.')    
    pylab.title(title)

    
def ensemble(M,T):
    """
    Function to integrate an ensemble of trajectories of the Lorenz system.
    Inputs:
    M - the number of ensemble members
    T - the time to integrate for
    """
    ens = 0.1*np.random.randn(M,3)
    

    for m in range(M):
        t = np.array([0.,T])
        if T > 0: # Cannot integrate if T=0. If T=0 then the ensemble is simply left intact
            data = integrate.odeint(vectorfield,y0=ens[m,:], t=t, args=(10.0,28.,8./3.),mxstep=100000)            
            ens[m,:] = data[1,:]        

    return ens
    
def f_m(d):
    """
    Function to apply ergodic average to.
    inputs:
    X - array, first dimension time, second dimension component
    outputs:
    f_m: - 1d- array of f values
    """
    return d[:,0]**2 + d[:,1]**2 + d[:,2]**2

def f(d):
    """
    Function to apply ergodic average to.
    inputs:
    X - array, first dimension time, second dimension component
    outputs:
    f: - 1d- array of f values
    """
    return d[0]**2 + d[1]**2 + d[2]**2
    
def getSpAvg(M,T,Dt):
    #Calculate the spatial average
    fx = np.zeros(M)
    for i in range(M):
        #Generate random initial conditions and integrate up to time T
        d,tim = getdata(np.random.randn(3),T,Dt)
        #Take f(X) for the above data        
        fx[i] = f(d[-1,:])
    SpAvg = np.sum(fx)/len(fx)  
    return SpAvg
    
def spatialAvg():
    
    return None

if __name__ == '__main__':

    # ---- Ex1 ------------------------------------------------------
    #show the ensemble at various integration times
    for time in [0,0.5,1,10,100]: #list of times
        Ens = ensemble(100,time)
        ensemble_plot(Ens,'T='+str(time))
    pylab.show()
    # ---------------------------------------------------------------
        
    # ---- Ex2 -------------------------------------------------------
    #Get data for the Lorenz sys    
    Dt = 1    
    #Generate our sample    
    Tp = [50,80,100,120]
    Mp = [10,20,25]
    S = np.zeros([len(Tp),len(Mp)])
    k=0
    l=0
    for T in Tp: #end time
        for M in Mp: #ensemble size
            y = np.zeros(M)
            #signal where we are
            print 'l=',l
            print 'k=',k
            for i in range(M):
                y[i] = getSpAvg(M,T,Dt)
            S[k,l] = np.var(y)
            l+=1
        k+=1
        l=0        
        
    plt.figure()
    plt.plot(Tp,S[:,0],'o-',label='M='+str(Mp[0]))   
    plt.plot(Tp,S[:,1],'o-',label='M='+str(Mp[1]))
    plt.plot(Tp,S[:,2],'o-',label='M='+str(Mp[2]))
    plt.xlabel('T')
    plt.ylabel('Var')  
    plt.legend(loc='best')
    plt.title('Variance evolution with increasing time')
    plt.show()
               
    #Calculate the variance in the approximation
    print 'Sample variance =',np.var(y)
    
    #Calculate the time average
    print 'Computing the time average ...'
    N = 400
    TAvg = np.zeros(N)
    for n in range(1,N):
        x0,tim = getdata(np.random.randn(3),n,2*Dt)           
        TAvg[n] = np.sum(f_m(x0))/n
           
    plt.figure()
    plt.plot(range(N), TAvg, '.-', label='Time average')
    plt.xlabel('N')
    plt.ylabel('Time average')
    plt.title('Convergence of the time average')
    plt.show()
    
    print 'The time averages converges quite quickly to a value of ~370.\
           After values of N>50 the time averages plateaus with some oscillations\
           around the convergence'
    print 'To demonstrate the independence of the time averege to the\
           initial conditions we only have to run this code again as the\
           initial conditions are given at random'
    
    #-----------------------------------------------------------------
    
   
    
    
