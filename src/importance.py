import numpy as np
import pylab
import matplotlib.pyplot as plt


"""
File containing code relating to Computational Lab 3 of the Dynamics
MPECDT core course.
"""

def square(x):
    """
    Function to compute the square of the input. If the input is 
    a numpy array, this returns a numpy array of all of the squared values.
    inputs:
    x - a numpy array of values

    outputs:
    y - a numpy array containing the square of all of the values
    """
    return x**4

def normal(N):
    """
    Function to return a numpy array containing N samples from 
    a N(0,1) distribution.
    
    inputs:
    N - number of samples
    
    outputs:
    X - the samples
    """
    return np.random.randn(N)
    
def clustered(N):
    """
    Function to return N samples from a clustered distribution as defined in Ex2
    
    Input
    N - number of samples
    
    Output
    X - the samples
    """
    #Get a sample from the uniform distribution
    s = np.random.rand(N)
    
    return -0.1*np.log(1-s+s*np.exp(-10))
    
def clusterPDF(x):
    """
    Function to evaluate the PDF for the clustered distribution. 
    If the input is a numpy array, this returns a numpy
    array of all of the squared values.

    inputs:
    x - a numpy array of input values
    
    outputs:
    y - a numpy array of rho(x)
    """
    y = 0*x    
    y[x>0] = (10.0*np.exp(-10*x))/(1.0-np.exp(-10))
    y[x>1] = 0.0
    return y

def normal_pdf(x):
    """
    Function to evaluate the PDF for the normal distribution (the
    normalisation coefficient is ignored). If the input is a numpy
    array, this returns a numpy array with the PDF evaluated at each
    of the values in the array.

    inputs:
    x - a numpy array of input values
    
    outputs:
    rho - a numpy array of rho(x)
    """
    return np.exp(-x**2/2)

def uniform_pdf(x):
    """
    Function to evaluate the PDF for the standard uniform
    distribution. If the input is a numpy array, this returns a numpy
    array of all of the squared values.

    inputs:
    x - a numpy array of input values
    
    outputs:
    rho - a numpy array of rho(x)
    """
    y = 0*x
    y[x>0] = 1.0
    y[x>1] = 0.0
    return y

def importance(f, Y, rho, rhoprime, N):
    """
    Function to compute the importance sampling estimate of the
    expectation E[f(X)], with N samples

    inputs:
    f - a Python function that evaluates a chosen mathematical function on
    each entry in a numpy array
    Y - a Python function that takes N as input and returns
    independent individually distributed random samples from a chosen
    probability distribution
    rho - a Python function that evaluates the PDF for the desired distribution
    on each entry in a numpy array
    rhoprime - a Python function that evaluates the PDF for the
    distribution for Y, on each entry in a numpy array
    N - the number of samples to use
    """
    
    #define a dummy variable for the samples of the random var Y
    _Y = Y(N)
    #There are some potential problems here with numpy so I set the 
    #error reporting settings to PRINT
    np.seterr(all='print')
    
    theta = np.sum(f(_Y)*rho(_Y)/rhoprime(_Y)) / np.sum(rho(_Y)/rhoprime(_Y))
      
    return theta
    
def f2(x):
    """
    The function as defined by the 2nd exercise. It works with single 
    number and arrays, depending on the shape of the input variable.
    
    Input
    x - a numpy array (can be of size 0)
    
    Output
    f - Returns exp(-10x)cos(x)
    """
    f = np.exp(-10.0*x)*np.cos(x)
    return f

if __name__ == '__main__':
    
    #---------------------------Ex1-------------------------------------
    #I skipped over calculating only one theta. Now I'm calculating how
    #the importance sampling is converging with increasing N
    
    #Parameters
    N = 1000
    #I changed the "squared" function to return x^4
    #So now I{0,1}(x^4) = 1/5
    T = 1.0/5.0
    
    Err = np.zeros(N-1)    
    for i in range(1,N):
        Err[i-1] = np.abs(importance(square,normal,uniform_pdf,normal_pdf,i)-T)       
              
    #loglog plot of the error of the estimate
    plt.figure(1)
    plt.loglog(np.arange(1,N),Err,'b-',label='$Err_1$')      
    plt.xlabel('N')
    plt.ylabel('$Err$')
    plt.title('Convergence of the Error (Ex1)')
    
    print '\n\nThere are some errors for small Ns the importance() function\
           so I set the error settings to print all the warnings. Also,\
           from the plot just seen we can say that the error of the estimate\
           is converging (slowly) to 0 with increasing values of N.\n\n'
    
    #plot both convergences on fig 3
    plt.figure(2)
    plt.loglog(np.arange(1,N),Err,'b-',label='$Err_1$')      
    plt.xlabel('N')
    plt.ylabel('$Err$')
    plt.title('Convergence of the Error (Ex1)')
    #-------------------------------------------------------------------
    
    #---------------------------Ex2-------------------------------------
    #Parameters
    N = 1000
    #The analytic value of the expectation
    T = 10.0/101.0 - (10*np.cos(1)-np.sin(1))/(101.0*np.exp(10))
    #Redefine the error of the estimate
    Err = np.zeros(N-1) 
    print 'Err=',Err.shape
    for i in range(1,N):
        Err[i-1] = np.abs(importance(f2,clustered,uniform_pdf,clusterPDF,i)-T)        
    
    plt.figure(2)
    plt.loglog(np.arange(1,N),Err,'r-',label='$Err_2$')
    plt.xlabel('N')
    plt.ylabel('$Err$')
    plt.title('Combined plot ')
    plt.legend(loc='lower left')  
    
    #loglog plot of the error of the estimate
    plt.figure(3)
    plt.loglog(np.arange(1,N),Err,'r-',label='$Err_2$')
    plt.xlabel('N')
    plt.ylabel('$Err$')    
    plt.title('Convergence of the Error (Ex2) ') 
    plt.show(block=False)   
    
    print '\n\nImportance sampling works in this example as shown by the convergence\
           plot in Fig. 2. Also, the two errors converge to 0 at different\
           rates. We would expect both to converge at a similar rare of 1/sqrt(N).\
           This is puzzling...\n\n'
    input('Press any key to continue...')
    #-------------------------------------------------------------------
