from scipy import integrate
import numpy as np
import pylab
import math

"""
File containing code relating to Computational Lab 1 of the Dynamics
MPECDT core course.

You should fork this repository and add any extra functions and
classes to this module.
"""

def rms(data, pred):
    #Calculate the root mean square error
    
    #create a new array to hold the rms error
    r = np.zeros(3)
    
    #loop through and calculate error
    for i in range(0,data.shape[0]):
        r[0] += np.power(np.abs(data[i,0]-pred[i,0]),2)
        r[1] += np.power(np.abs(data[i,2]-pred[i,1]),2)
        r[2] += np.power(np.abs(data[i,2]-pred[i,2]),2)
    
    r[0] = np.sqrt((1/data.shape[0])*r[0])
    r[1] = np.sqrt((1/data.shape[0])*r[1])
    r[2] = np.sqrt((1/data.shape[0])*r[2])
    
    return r

def predict_linear(orbit):
    #Check for orbit size
    if orbit.shape[1] !=3:
        raise TypeError("In predict_linear(), size of orbit should be (# x 3) at least")
        return None
    #initialise and instantiate the predicted orbit as a zeros vector
    pred = np.zeros(orbit.shape)
    
    #start at 2 points into the orbit
    for k in range(2,orbit.shape[0]):
        px = 2*orbit[k-1,0]-orbit[k-2,0]
        py = 2*orbit[k-1,1]-orbit[k-2,1]
        pz = 2*orbit[k-1,2]-orbit[k-2,2]
        pred[k,0] = px
        pred[k,1] = py
        pred[k,2] = pz
    
    #Fill in the first 2 zeros in predict to match the orbit
    pred[0] = orbit[0]
    pred[1] = orbit[1]
        
    return pred

def vectorfield(x, t, alpha, beta):
    """
    Function to return the value of dx/dt, given x.
    
    Inputs:
    x - the value of x
    t - the value of t
    alpha - a parameter
    beta - another parameter

    Outputs:
    dxdt - the value of dx/dt
    """    
    return alpha*x + beta
    
def LorenzSys(x, t, sigma, rho, beta):
	# x = x[0]
	# y = x[1]
	# z = x[2]
    
    #make sure there are enough elements in x
    if len(x) !=3:
        raise TypeError("In LorenzSys, x must be an array of exactly 3 elements.")
        return (None,None,None)    
    
    return np.array([sigma*(x[1]-x[0]), x[0]*(rho-x[2])-x[1], x[0]*x[1]-beta*x[2]])

def getdata(y0,T,Deltat):
    """
    Function to return simulated observational data from a dynamical
    system, for testing out forecasting tools with.

    Inputs:
    y0 - the initial condition of the system state
    T - The final time to simulate until
    Deltat - the time intervals to obtain observations from. Note
    that the numerical integration method is time-adaptive and
    chooses it's own timestep size, but will interpolate to obtain
    values at these intervals.
    """
        
    t = np.arange(0.,T,Deltat)
    
    #The args for the Lorenz system are hard-coded
    data = integrate.odeint(LorenzSys,y0, t=t, args=(10,28,8/3))    
        
    return data,t


def plot3D(orbit,pred):
    """
    Make a 3D plot of data.
    Inputs:
    x - Numpy array of x coordinate
    y - Numpy array of y coordinate
    z - Numpy array of z coordinate
    """
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(orbit[:,0],orbit[:,1],orbit[:,2],label='LorenzSys')
    ax.plot(pred[:,0],pred[:,1],pred[:,2],label='Prediction')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    ax.legend()
    pylab.show()
    
#Dynamics Tutorial 2 Exercise 1.
#Perturb the initial conditions of the Lorenz Sys by 1% and plot a 
#graph of the distance between the two trajectories
#pert - the perturbation of the system in percentage
#Return: distance
def dynT2Ex1(pert):
    #check if pert is a percentage
    if pert <0 or pert >1:
        raise TypeError("In dynEx1, The perturbation must be a percentage.")
        return None
        
    #system parameters
    sigma = 10
    rho= 28
    beta = 8/3
    #unperturbed initial conditions
    x = -0.587
    y = -0.563
    z = 16.870
    #perturbed init cond 
    dx = x+ x * pert
    dy = y+ y * pert
    dz = z+ z * pert
    
    #get the data for the two initial conditions
    ud,ut = getdata([x, y, z],100,0.05)
    pd,pt = getdata([dx,dy,dz],100,0.05)
    
    #calculate the distance between the trajectories
    dist = np.zeros(ud.shape[0])
    for i in range(0,len(dist)):
        dist[i] = np.sqrt(np.power(ud[i,0]-pd[i,0],2)+np.power(ud[i,1]-pd[i,1],2)+np.power(ud[i,2]-pd[i,2],2))
    
    import matplotlib.pyplot as plt
    #plot the distance as seilog-y plot
    plt.semilogy(ut, dist)
    plt.title('Distance between perturbed and unperturbed orbits')
    plt.show()
    
    return 1
    
def dynT2Ex2():
    return 1
        
#If this file is called as a main execute this
if __name__ == '__main__':
    
    md,mt = getdata([-0.587, -0.563, 16.870],100,0.05)
    #get the prediction for this orbit
    p = predict_linear(md)
    #calculate the rms of the predicted orbit
    #err = rms(md, p)
    #print err
    #print 'Error=',err[0],err[1],err[2]
    
    #pylab.plot(mytime,mydat)
    #plot3D(md,p)
    
    #DynTutorial 2 Ex 1
    #dynT2Ex1(0.01)
    
    
    
