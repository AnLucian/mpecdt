.. default-role:: math

Computational Lab: Data and Uncertainty 5
=============================

This is the tutorial material for Computational Lab 5 in the Data and
Uncertainty core course of the MPECDT MRes.

As for all of the computational labs in this course, you should
develop your solutions to these exercises in a git repository, hosted
on BitBucket. During the timetabled laboratory: 

- Work through the exercises on your laptop, by forking and checking out
  a copy of the mpecdt git repository. 
- If you would like help with issues in your code, either:

  - Paste the code into a gist, or
  - Push your code up to your forked git repository on BitBucket.
    Then, share the link to your gist or repository in the IRC channel

.. container:: tasks  

   Create a file in your repository called ``lorenz-mcmc.py``, in
   which you should add all of the tasks from this tutorial. The
   Exercise below will be assessed as part of the coursework for this
   project. Solutions to the exercises should execute as part of the
   script; use print statements to answer any questions in the
   exercises.  Please email ``colin.cotter@imperial.ac.uk`` the git
   revision for your repository in the form of a SHA (just type ``git
   log`` to get the list of SHAs for each revision) before midnight GMT
   on 20th January 2014.

This Computational Lab is about data assimilation with the Lorenz
model using the pCN algorithm.

We will continue to look at solving inverse problems, now in the
context of dynamical models. The setting is the following. We have a
dynamically evolving system (such as the atmosphere, or a virus
spreading through a population) and incomplete, imperfect measurements
of it are obtained at various times. What is our estimate of the 
PDF for the initial condition?

We will conduct experiments using the Lorenz model. In our setting, we
will take noisy observations of all three components
`(x(t_n),y(t_n),z(t_n))` at various intervals `n=1,\ldots,N`. In that
case, the forward operator takes initial conditions `(x_0,y_0,z_0)` as
input, and returns a size `3N` vector as output, containing the
observations of all three components. We will continue to use a Normal
noise model, with mean 0 and variance `\sigma^2=0.1^2`.

Exercise 1
----------

.. container:: tasks

   Using the syntax from the previous tutorial, implement a forward
   model function based on the Lorenz model, which takes the initial
   conditions as input, and return values of `(x,y,z)` at various
   times as output. Choose as an initial condition the value
   `(x,y,z)=(-0.587,-0.563,16.870)`. In the case of a single
   observation at `t=1`, apply the forward model, and add observation
   noise to the output, independently for each component and with
   variance given above. Now we assume that we don't know what the
   initial condition is precisely, and say that our prior distribution
   is independent for each component, with mean `(-0.5,-0.55,17)`, and
   with variance `\sigma^2=0.5^2` separately in each component.

   Use the pCN algorithm to obtain samples from the posterior
   distribution for the initial condition. If you want to use the
   ``mcmc()`` function from the previous lab, add ``from mcmc import
   mcmc`` to the top of your file. You can similarly do this with any
   other functions from the computational labs (don't forget to make
   sure any changes to these functions have been pushed to BitBucket
   otherwise they won't execute properly when I'm marking them). 

   Don't forget to start by checking the average acceptance probability,
   and tuning `\beta` so that it takes a value of around 0.25. 

   After you have discarded the burn-in samples, inspect the
   distribution of samples obtained from the algorithm. Visualising
   PDFs in 3D is hard, so take a look at the PDFs for each component
   separately.  Are the components correlated? Evaluate the mean and
   covariance matrix for this PDF. Investigate the influence of the
   choice of observation and prior variances on the performance of the
   algorithm, and the mean and covariance of the initial condition
   PDFs. Observe the PDF of `f((x,y,z))`. Is it consistent with the noise
   model?

   Now extend your investigation to look at observations at multiple
   instances in time, say 10 observations between time 0 and 5. How
   does the variance of your posterior distribution vary as more
   observation values are added in this fixed interval?

As can be seen, the pCN algorithm can accurately recover the posterior
distribution, but can be very expensive since many samples are
required. More practical algorithms make use of multilevel and
multigrid methods, and make approximations and assumptions about the
distributions being used. It is also the case that the algorithm is
not sequential, meaning that it is not amenable in this form to
updating the PDFs when new data becomes available at later times.
There are also problems of model error, when the model is not perfect
as we assumed in these tutorials. This is an exploding field and we
encourage you to get involved!







