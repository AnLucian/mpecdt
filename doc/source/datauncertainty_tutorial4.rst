.. default-role:: math

Computational Lab: Data and Uncertainty 4
=============================

This is the tutorial material for Computational Lab 4 in the Data and
Uncertainty core course of the MPECDT MRes.

As for all of the computational labs in this course, you should
develop your solutions to these exercises in a git repository, hosted
on BitBucket. During the timetabled laboratory: 

- Work through the exercises on your laptop, by forking and checking out
  a copy of the mpecdt git repository. 
- If you would like help with issues in your code, either:

  - Paste the code into a gist, or
  - Push your code up to your forked git repository on BitBucket.
    Then, share the link to your gist or repository in the IRC channel

.. container:: tasks  

   After merging the latest version of the mpecdt repository (use
   ``git merge mpecdt/master``), you will find a file to the
   mpecdt/src directory called ``mcmc.py``, in which you should add
   all of the tasks from this tutorial. This lab is not assessed, but
   will prepare you for lab 5, which will be assessed.

This Computational Lab is about:

#. How to formulate a Bayesian inverse problem,
#. How to solve that inverse problem using a Monte Carlo Markov Chain.

Inverse problems are ubiquitous throughout geosciences (and
beyond). The idea is that we have some incomplete and imperfect
measurements of a physical system, plus a predictive model of it, and
we wish to combine these together to obtain our best estimate of the
system state.

For example, say that we have satellite data which gives the velocity
of the Greenland ice sheet at a set of points on the surface, with
some measurement errors due to atmospheric turbulence. Say that we
also have a model the predicts the motion of the ice sheet, given
knowledge of the topography (shape of the ground) under the ice, as
well as the friction coefficient which is a function of space since it
depends on the properties of the rocks underneath the ice. We would
like to use the satellite data to improve our estimates of the
topography and friction coefficient fields. How can this be done?

More generally, we assume that we have:

1. A model of a system (which we call the forward model), represented
   by a function `y=f(x)`, that takes in inputs `x\in \mathbb{R}^n`
   and returns values `y\in \mathbb{R}^m`, where `n` and `m` need not
   be equal. This model may be deterministic or stochastic.
2. A statistical model of the observation process including random
   observation error, `y'=O(y;\omega)`. For example, `y'=y+\epsilon` where
   `\epsilon\sim N(0,\sigma)`.
3. Observed data `\hat{y}`.

If we know `x`, or know the probability distribution for `x`, then we
can predict the probability distribution for `y'` by combining
the forward model and observation model. However, it is often the 
situation that we have observed `\hat{y}`, and we wish to estimate
a probability distribution for `x`. 

This is a conditional probability distribution, with PDF written as
`\pi(x|\hat{y})`, i.e. the probability distribution for `x` given that
we observe `\hat{y}`. We call this distribution the *posterior*
distribution for `x` (because it is the distribution *after* we know
about `\hat{y}`). We can compute this PDF using Bayes' Formula.

.. math::
   \pi(x|\hat{y}) = \frac{\pi_L(\hat{y}|x)\pi_X(x)}{\pi_Y(\hat{y})},

where:

* `\pi_L(\hat{y}|x)` is the *likelihood*, which is the probability of
  observing `\hat{y}` given that the input is `x` (this can be
  computed using the statistical observation model).
* `\pi_X(x)` is the *prior*, which is the probability distribution for
  the input being `x` without knowledge of the observations. This 
  prior is chosen according to our previous knowledge of the system, 
  from experience or physical insight.
* `\pi_Y(\hat{y})` is the probability distribution for the observation without knowledge of `\hat{y}`. This is the hardest to compute, but is only a normalisation factor that can be defined by saying that the posterior distribution must integrate to 1 over `x`.

Having obtained the posterior distribution, we would like to compute
statistics, such as the mean and variance, or compute histograms.
Often this is tricky, since it involves intractable high-dimensional
integrals, or even integrals where we cannot explicitly write down the
integrand since it involves the output of a complex computer model
(such as a weather model). Hence, we resort to Monte Carlo methods.
Often it is not possible to directly simulate samples from the
posterior distribution; one approach is to use importance sampling
(this becomes very useful when we are making sequential forecasts as
part of a rolling observational system where new data comes in at
regular intervals and we want to issue up-to-date forecasts). In 
this lab we will consider an alternative approach which is to use 
Markov Chain Monte Carlo (MCMC).

So far we have only considered Monte Carlo methods where each sample
is independent. The idea behind Markov chain methods is that we make
use of information from the previous sample to improve convergence.
We end up with a correlated sequence of samples `\{X_i\}_{i=1}^N` for
the random variable `X` which predicts the value of `x`, which can be
then used to compute approximate averages

.. math::
   \mathbb{E}\left[f(X)\right] = \frac{1}{N}\sum_{i=1}^N f(X_i).


The following algorithm implements a Monte Carlo Markov Chain for our
Bayesian inverse problem, using a version of the Metropolis-Hastings
algorithm called the pCN algorithm (see Stuart (2010)).  We assume
that the observation noise is Gaussian with each component of `y`
being independent, and with observation noise variance `\sigma^2`, so
that the likelihood is

.. math::
   \pi(y|x) \propto \exp\left(-\Phi(x)\right), \quad \Phi(x) = \left(f(x)-y\right)^2/\sigma^2.

1. Start with an initial condition `x_0`.
2. ``for i in range(N):``

   1. Draw a sample `w` from the prior distribution `\pi_X(x)`, and compute the "proposal" `v=(1-\beta^2)^{1/2}x_i + \beta w`.

   2. Compute `a(x_i,v) = min(1,\exp(\Phi(x_i)-\Phi(v))`.

   3. Draw a sample `u` from the `U([0,1])` uniform distribution.

   4. If `u<a(x_i,v)` then set `u_{i+1}=v` (we say that the proposal has been *accepted*), else set `u_{i+1}=u_i` (we say that the proposal has been *rejected*).

Here, `0<\beta<1` is a parameter which must be tuned to optimise
convergence in the algorithm. It has been proved that this algorithm
has optimal convergence if the average value of the acceptance
probability `a(x_i,v)` is around 0.25. A high value of `\beta` leads
to a lower average acceptance probability, so `\beta` can be tuned to
obtain optimal convergence before computing statistics. It is also the
case that if the initial condition is not very probable, then the
algorithm takes a while to "burn in" before the algorithm reaches
statistical equilibrium. This burn in stage is characterised by a
drift in the rolling average of the acceptance probability; the
algorithm is in equilibrium when the average acceptance probability
settles down to a constant value. Hence, it is necessary to discard
the samples obtained during the burn in period.

Exercise 1 
----------

.. container:: tasks  

   The file ``mcmc.py`` contains an incomplete implementation of this
   algorithm. Finish the mcmc() function in this file so that it
   implements the pCN algorithm above. The idea is that the function
   takes arbitrary priors, observation models and forward models.

In the case where the forward model is linear, and the observation
model and prior are both Normal distributions, the posterior is
also normal. Hence, to verify the code, we'll take `f(x)=x`,
`N(0,1)` for the prior, and `N(0,1)` for the observation noise
model, i.e.

.. math::
   \pi_L(y|x) \propto \exp\left(-(y-x)^2/2\right).

Then, completing the square (check this!) leads to

.. math::
   \pi(x|\hat{y}) \propto \exp\left(-(x-y/2)^2\right),

hence the prior has mean 0.5 and variance `1/\sqrt{2}`. We'll take
`\hat{y}=1`.

.. container:: tasks  

   Using this inverse problem setup, compute the cumulative average of
   `a(x_i,v)` and plot it as a function of `i`. It should start to
   asymptote to a constant value once the algorithm reaches
   equilibrium. Then, tune `\beta` until the asymptotic value is
   approximately 0.25. Discard the samples that are taken before the
   average acceptance rate has settled down, and compute the mean and
   the variance of the remaining samples. Compare these with the
   correct answers given above. Visualise the PDF of the samples. Does
   it look like a Normal distribution? For a single Markov chain
   sequence of samples, plot the error as a function of the number of
   samples.  Approximately how does it vary as a function of number of
   samples?

Exercise 2
----------

.. container:: tasks
	       
   Now choose `f(x)=\exp(x)`, a `N(0,1)` prior and `N(0,0.1^2)` noise
   model, with `\hat{y}=1`. You will need to retune `\beta` since the
   mapping between `\beta` and mean acceptance rate is
   problem-dependent. You will also need to find the burn-in time
   again and discard an appropriate number of samples. Plot the PDF of
   the samples. Does it make intuitive sense in terms of the solution
   to the inverse problem?  Plot the PDF of `f(x)`. Is it consistent
   with the noise model?

Exercise 3
----------

This type of standard MCMC algorithm can get into difficulties when
there is low observation error and multiple maxima in the PDF. In
particular, it can get stuck near one maxima for a very long time.
Various MCMC algorithms have been proposed to avoid this problem.

.. container:: tasks
   
   Keeping the same noise model and prior as in Exercise 2, change the
   forward model to `f(x)=x^2`, with `\hat{y}=1`. Why do we expect
   multiple maxima in this case? Repeat the MCMC calculation multiple
   times, plotting PDFs of `x`. What do you observe?

References
----------

*A. M. Stuart.* Inverse problems: A Bayesian perspective. Acta Numerica,
   19:451–559, 2010.



